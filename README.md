![<img src="http://c.asstatic.com/asuserimages/oxfordmarketing_0-230x180.jpg" />](https://bitbucket.org/repo/daa9gob/images/1214003818-oxford-marketing-logo-square.png)

[Oxford Marketing](http://oxfordmarketingus.com/) handles all your experiential marketing & event staffing needs as one of the top brand ambassador agencies in the country.

**Contact us:**

**Address:** 710 Walnut Avenue, Bohemia, NY, 11716, USA

**Phone:** (844) 488-9636

**Email:** info@oxfordmarketingus.com

**Owner**: Andrew Sirignano


**Connect with us:**

[Facebook](https://www.facebook.com/oxfordmarketingus/)
[Twitter](https://twitter.com/oxfordstaff)
[Youtube](https://www.youtube.com/channel/UCOptpI-ScmqqmaugUia3s7Q)
[Pinterest](https://www.pinterest.com/oxfordmarketing/)
[[LinkedIn](https://www.linkedin.com/company/oxford-marketing)
[G+](https://plus.google.com/u/0/109906846166046239900)